# Generate code documentation in HTML-format using Doxygen

Use the next commands to install the necessary **Doxygen** tooling:

```bash
sudo apt-get update
sudo apt install doxygen
sudo apt install doxygen-gui
sudo apt-get install graphviz
```

Goto the next directory **...../fccfMQTT-docs**

In this directory the file Doxyfile is found.
This file contains the configuration data for generating the documentation.

## Start the doxygen wizard

```bash
doxywizard Doxyfile
```

Always use **relative paths** for INPUT files.

By selecting Run tab the documentation will be generated in the subdirectory **html** in html format.

## Use Doxyfile in other projects

Use a copy of the file **Doxyfile** in other projects and update the contents according to the directories and files used.
