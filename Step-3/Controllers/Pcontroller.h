#ifndef PCONTROLLER_H
#define PCONTROLLER_H

#include "Controller.h"

/// Pcontroller represent a Proportional controler.
/// Pcontroller is_a Controller.
class Pcontroller : public Controller
{
public:
   Pcontroller(double Tsample, double P);
   virtual ~Pcontroller() = default;
   double operator()(double err) override;

private:
   double P_;
};

#endif // PCONTROLLER_H
