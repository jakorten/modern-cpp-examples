// C uses call by value for passing function arguments.
// We need a call by reference for changing an argument value
// outside the function: pointers in C are necessary.
//
// C++: new syntax for call by reference, we can avoid pointers!
//

#include <iostream>

// Call by value
void swapByValue(int a, int b);
// C++ call by reference
void swap(int &a, int &b);

using namespace std;

int main()
{
   int x = 2;
   int y = 3;

   cout << "Initial values      x = " << x << " y = " << y << "\n\n";
   swapByValue(x, y);
   cout << "swapByValue(x, y);  x = " << x << " y = " << y << "\n\n";

   // Call by reference
   swap(x, y);
   cout << "swap(x, y);         x = " << x << " y = " << y << "\n\n";

   return 0;
}

void swapByValue(int a, int b)
{
   int temp = a;
   a = b;
   b = temp;
}

// Call by reference, no pointers anymore in the finction implementation
void swap(int &a, int &b)
{
   int temp = a;
   a = b;
   b = temp;
}
