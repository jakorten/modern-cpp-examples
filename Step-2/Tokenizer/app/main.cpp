#include "tokenizer.h"
#include <iostream>

int main()
{
   // String containing ints and doubles
   // Uniform initialisation
   std::string line{" 1  2   33.337  "};
   std::cout << "Input: '" << line << "'" << std::endl;

   // Using default delimiter: isspace (see tokenizer.h)
   std::vector<std::string> tokens{split(line)};

   for (const auto &tkn : tokens) {
      std::cout << tkn << std::endl;
   }
   std::cout << std::endl;

   // Conversion of string tokens to int and double types
   // Uniform initialisation
   int i1{stoi(tokens[0])};
   int i2{stoi(tokens[1])};
   double d3{stod(tokens[2])};

   std::cout << i1 << " " << i2 << " " << d3 << std::endl
             << std::string(50, '-') << std::endl;

   // Example: using a MQTT topic string (is like a directory path)
   std::string topic{"ESEiot/1819feb/RPI0200/temperature"};
   std::cout << "Topic: '" << topic << "'" << std::endl;
   std::vector<std::string> tokensTopic{split(topic, isForwardSlash)};

   for (const auto &tkn : tokensTopic) {
      std::cout << tkn << std::endl;
   }
   std::cout << std::endl
             << tokensTopic[0] << "  " << tokensTopic[1] << "  "
             << tokensTopic[2] << std::endl
             << std::string(50, '-') << std::endl
             << std::endl;

   return 0;
}
