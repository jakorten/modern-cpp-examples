#include <iostream>
#include <string>

using namespace std;

int main()
{
   int n{70};

   cout << "Formatted output streams:" << endl << endl;
   cout << "n = " << dec << n << " hex = " << hex << n << endl;
   cout << "n = " << dec << n << " dec = " << dec << n << endl;
   cout << "n = " << dec << n << " oct = " << oct << n << endl;
   cout << dec << endl;

   cout << string(75, '-') << endl;
   cout.flags(ios::right | ios::hex | ios::showbase);
   cout.width(75);
   cout << 100 << endl << endl;

   cout << string(25, '-') << endl;
   cout.flags(ios::right | ios::oct | ios::showbase);
   cout.width(25);
   cout << 100 << endl << endl;

   cout << string(25, '-') << endl;
   cout.flags(ios::right | ios::dec | ios::showbase);
   cout.width(25);
   cout << 100 << endl << endl;

   // Booleans in text format or value format
   bool bft{true};

   cout << "boolean bft = " << boolalpha << bft << endl;
   cout << "boolean bft = " << noboolalpha << bft << endl;
   cout << endl;

   // Formatting doubles
   double a{3.1415926534};
   double b{2006.0};
   // Scientific notation:
   // e mutiplied by the number 10 raised to the power of exponent
   double c{1.0e-10};

   // Set floating-point decimal precision
   cout.precision(5);
   cout << a << '\t' << b << '\t' << c << endl;
   cout << fixed << a << '\t' << b << '\t' << c << endl;
   cout << scientific << a << '\t' << b << '\t' << c << endl;
   cout << endl;

   return 0;
}
