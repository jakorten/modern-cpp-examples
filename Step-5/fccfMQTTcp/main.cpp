// fccfMQTTcp: MQTT client, using CommandProcessor base class

#include "AppInfo.h"
#include "MQTTconfig.h"
#include "TemperatureConverter.h"

#include <atomic>
#include <csignal>
#include <iostream>
#include <sstream>

std::string MosquittoLibVersion();

volatile sig_atomic_t receivedSIGINT{false};

/// Signal handling routine
void handleSIGINT(int /* s */)
{
   receivedSIGINT = true;
}

int main(int argc, char *argv[])
{
   try {
      signal(SIGINT, handleSIGINT);

      std::string mqttBroker{MQTT_LOCAL_BROKER};
      int mqttBrokerPort{MQTT_LOCAL_BROKER_PORT};

      switch (argc) {
         case 1:
            // Using MQTT_LOCAL_BROKER and MQTT_LOCAL_BROKER_PORT
            break;
         case 2:
            // Using MQTT_LOCAL_BROKER_PORT
            mqttBroker = std::string(argv[1]);
            break;
         case 3:
            mqttBroker = std::string(argv[1]);
            mqttBrokerPort = std::stoi(argv[2]);
            break;
         default:
            std::cerr << "\nERROR command line arguments: "
                         "cpMQTT <URL broker> <broker port>\n";
            exit(EXIT_FAILURE);
      }

      std::cout << "-- MQTT application: " << APPNAME_VERSION << "  ";
      mosqpp::lib_init();

      std::cout << "\n   uses Mosquitto lib version " << MosquittoLibVersion()
                << "\n";

      // First MQTT client.
      TemperatureConverter tccp("Tc", "tccp", mqttBroker, mqttBrokerPort);

      // Checking rc for reconnection, 'clients' is an initializer_list
      auto clients = {static_cast<mosqpp::mosquittopp *>(&tccp)};

      while (!receivedSIGINT) {
         for (auto client : clients) {
            int rc = client->loop();
            if (rc) {
               std::cerr << "-- MQTT reconnect" << std::endl;
               client->reconnect();
            }
         }
      }
   }
   catch (std::exception &e) {
      std::cerr << "Exception " << e.what() << std::endl;
   }
   catch (...) {
      std::cerr << "UNKNOWN EXCEPTION\n";
   }

   std::cout << "-- MQTT application: " << APPNAME_VERSION << " stopped\n\n";

   mosqpp::lib_cleanup();

   return 0;
}

std::string MosquittoLibVersion()
{
   int major{0};
   int minor{0};
   int revision{0};
   mosqpp::lib_version(&major, &minor, &revision);

   std::stringstream s;
   s << major << '.' << minor << '.' << revision;

   return s.str();
}
