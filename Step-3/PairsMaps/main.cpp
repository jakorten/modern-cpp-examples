// C++11 STL pairs and maps

#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <utility>

using namespace std;

// Commands related functions
int start(int data);
int stop(int data);
int repeat(int data);

// Commands related class member functions
class Commands
{
public:
   int update(int data)
   {
      cout << "exec Commands::update(" << data << ") ";
      return data;
   }
};

// C++11 instead of typedef, using STL function template
using commandfunction = std::function<int(int)>;

int main()
{
   pair<string, int> si{"one", 1};
   pair<int, int> ii{10, 100};

   cout << "Some mappings:" << endl;
   cout << si.first << " --> " << si.second << endl;
   cout << ii.first << " --> " << ii.second << endl << endl;

   pair<string, commandfunction> cf{"start", start};

   cout << "Command = '" << cf.first << "' executed: " << cf.second(11) << endl;

   // Instantiate Commands object cmds
   Commands cmds;
   // auto update = std::bind(&Commands::update, &cmds, placeholders::_1);
   // Replaced bind by lambda function:
   auto update = [&cmds](int input) { return cmds.update(input); };

   // map contains sorted pairs
   map<string, commandfunction> cfs{
      {"stop", stop}, {"start", start}, {"update", update}};

   cout << "Sorted contents of cfs map commands (key value):\n";
   int index{1};
   for (const auto &cmd : cfs) {
      cout << index++ << ". " << cmd.first << endl;
   }
   cout << endl;

   // Calling functions by the map interface
   cfs["start"](1000);
   cout << endl;
   cfs["update"](2000);
   cout << endl;
   cfs["stop"](3000);
   cout << endl;

   // Add new function
   cfs["repeat"] = repeat;
   // Call added function
   cfs["repeat"](2000);
   cout << endl;

   // ------------------------------------- Command processor
   string input;
   const string exitCommand("exit");
   do {
      cout << "\n\tEnter command: ";
      cin >> input;

      if (input != exitCommand) {
         auto iter_command = cfs.find(input);
         if (iter_command != end(cfs)) {
            int dataCommand{0};
            auto fnc = (*iter_command).second;

            cout << "\tInput data (int)? ";
            cin >> dataCommand;
            cout << "\tExecuting function: "
                 << fnc(dataCommand) << endl;
         } else {
            cerr << "+++ ERROR unkown command '" << input << "'";
         }
      }
   } while (input != exitCommand);
   cout << endl;

   return 0;
}

int start(int data)
{
   cout << "exec start(" << data << ") ";
   return data;
}

int stop(int data)
{
   cout << "exec stop(" << data << ") ";
   return data;
}

int repeat(int data)
{
   cout << "exec repeat(" << data << ") ";
   return data;
}
