/*!

\mainpage Compiling and executing fccfMQTTcp

MQTT client using the CommandProcessor class for calculating temperature conversions:
- Celsius to Fahrenheit
- Fahrenheit to Celsius (not yet implemented)

\section sec1 Compiling fccfMQTTcp

This application must be compiled by a makefile using the make command 
executing \link Makefile \endlink.

fccfMQTTcp uses the Mosquitto C and C++ library, this library must be installed.

\section sec2 Executing fccfMQTTcp

Use the next commands and options to start the application. Some examples:

- host = 127.0.0.1 (default, local broker)
- port = 1883 (default)

\verbatim ./fccfMQTTcp \endverbatim

- host = broker.hivemq.com  
- port = 1883 (default)

\verbatim ./fccfMQTTcp  broker.hivemq.com \endverbatim

- host = RPI2000.local  
- port = 5000

\verbatim ./fccfMQTTcp  RPI2000.local  5000 \endverbatim

After starting fccfMQTTcp will try to connect to the selected MQTT broker.

The application will show a lot of logging and debug information to stdout
and stderr. This can be switched off by changing the related code.

\section sec3 Examples Mosquitto publishing and subscribing

The next examples depend on the hostname of the MQTT client.
The hostname is used in the MQTT client id and the command topic text.

Some info is shown at the startup of the application using a local broker
(127.0.0.1 port 1883):

\verbatim 
-- MQTT application: fccfMQTTcp v3.4.0  uses Mosquitto lib version 1.5.0
CommandProcessor command topic: ESEiot/1819sep/Eos/Tc/tc/command
TemperatureConverter::TemperatureConverter()
    connect() host = '127.0.0.1'  port = 1883  id = EosTctc  topic root = ESEiot/1819sep/Eos
CommandProcessor::registerCommand  c2f
\endverbatim

In the message is the command name found and 0 or more parameters separated by spaces.
Use double quotes for messages containing white space (tabs and spaces).

Mosquitto publish command for the c2f command:

\verbatim mosquitto_pub -t ESEiot/1819sep/Eos/Tc/tc/command -m "c2f 20" 
\endverbatim

Mosquitto subscribe command:

\verbatim 
mosquitto_sub -v -t ESEiot/#
ESEiot/1819sep/Eos/Tc/tc/command/c2f/info  registered
ESEiot/1819sep/Eos/Tc/tc/command c2f 20
ESEiot/1819sep/Eos/Tc/tc/command/c2f/return 68.000000
\endverbatim

The topic used in the execution of the related command function:

\verbatim 
ESEiot/1819sep/Eos/Tc/tc/command/c2f/return
\endverbatim

This version is not yet checking the correct syntax of the parameters!

*/
