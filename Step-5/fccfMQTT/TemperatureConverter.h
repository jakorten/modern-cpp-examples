#ifndef TEMPERATURE_CONVERTER_H
#define TEMPERATURE_CONVERTER_H

#include <mosquittopp.h>
#include <string>

/// The class TemperatureConverter is an MQTT client class, public derived
/// from the base class mosqpp::mosquittopp.
/// TemperatureConverter is a derived class. 
/// Virtual functions declared in the base class need to be overridden 
/// (re-defined) in the derived class.
class TemperatureConverter : public mosqpp::mosquittopp
{
public:
   TemperatureConverter(const std::string &appname,
                        const std::string &clientname, const std::string &host,
                        int port);

   /// RAII: Resource Aquisition Is Initialisation
   /// In ctor connect(), in dtor disconnect()
   virtual ~TemperatureConverter();

protected:
   const std::string className_;
   const std::string mqttID_;

   /// \note C++11: override (compiler check), 
   /// virtual void on_connect(int rc) override;
   /// Shorter: virtual can be left away
   void on_connect(int rc) override;
   void on_disconnect(int rc) override;
   void on_message(const struct mosquitto_message *message) override;
   void on_subscribe(int mid, int qos_count, const int *granted_qos) override;
   void on_log(int level, const char *str) override;
   void on_error() override;
};

#endif
