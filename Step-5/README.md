# C++ MQTT clients

---

## Install MQTT Mosquitto library and commands

The following libs must be installed, before you can compile the **mosquitto** library.
Use **sudo apt install ....**

- libssl-dev
- libc-ares-dev
- uuid-dev
- libwebsockets-dev

if compiling of the **mosquitto** lib fails.

Steps after dowloading mosquitto from [Mosquitto download](https://mosquitto.org/download) in **tar.gz** format.
Unzip this package.

Websockets provide full-duplex communication channels over a single TCP connection.
For enabling websocket communication we must edit the __config.mk__ file: the websockets option must be set to "yes":

```bash
WITH_WEBSOCKETS:=yes
```

Go to the directory containing the __mosquitto__ code. Compile and configure commands:

```bash
make
sudo make install
sudo ldconfig
```

Ignore the **depricated** warnings.

Copy as root __mosquitto.conf.example__ to __mosquitto.conf__.
Update __mosquitto.conf__ for enabling websockets using port 9001:

```bash
sudo nano /etc/mosquitto/mosquitto.conf
```

Starting Mosquitto broker (as user, not as root):

```bash
mosquitto -c /etc/mosquitto/mosquitto.conf
```

---

## fccfMQTT application

Pre condition: a local MQTT broker must be started.

### Startup fccfMQTT without any command line arguments

Start a Bash shell \<Ctrl>\<Alt>\<t>.
Hostname is **Eos**, this name is used in the topic strings!

```bash
./fccfMQTT

-- MQTT application: fccfMQTT v3.2.1  uses Mosquitto lib version 1.6.3
TemperatureConverter::TemperatureConverter()
   connect() host = '127.0.0.1'  port = 1883  id = Eosfccftempconv  topic root = ESEiot/1920sep/Eos
TemperatureConverter::on_log()
   level = 16: Client Eosfccftempconv sending CONNECT
TemperatureConverter::on_log()
   level = 16: Client Eosfccftempconv received CONNACK (0)
TemperatureConverter::on_connect()
   connected with rc = 0
TemperatureConverter::on_log()
   level = 16: Client Eosfccftempconv sending SUBSCRIBE (Mid: 1, Topic: ESEiot/1920sep/Eos/celsiusQ, QoS: 0, Options: 0x00)
TemperatureConverter::on_connect()
    ESEiot/1920sep/Eos/celsiusQ
TemperatureConverter::on_log()
   level = 16: Client Eosfccftempconv received SUBACK
TemperatureConverter::on_subscribe()
   subscription succeeded, mid = 1 qos_count = 1 granted_qos = 0
```

### Subscribe for fccfMQTT

Start another Bash shell and subscribe to the local broker:

```bash
mosquitto_sub -t ESEiot/1920sep/#
```

### Publish for fccfMQTT

Start again another Bash shell. Publish to local broker:

```bash
mosquitto_pub -t ESEiot/1920sep/Eos/celsiusQ -m 23
```

### Result of subscription for fccfMQTT

See in the Bash shell used for subscribtion:

```bash

ESEiot/1920sep/Eos/fahrenheitA 73.400000
```

---

## fccfMQTTcp application

Pre condition: local MQTT broker must be started.
Hostname is **Eos**, this name is used in the topic string!

```bash
./fccfMQTTcp

-- MQTT application: fccfMQTTcp v3.4.1  uses Mosquitto lib version 1.6.3
CommandProcessor command topic: ESEiot/1920sep/Eos/Tc/tc/command
TemperatureConverter::TemperatureConverter()
    connect() host = '127.0.0.1'  port = 1883  id = EosTctc  topic root = ESEiot/1920sep/Eos
CommandProcessor::registerCommand  c2f
```

### Publish for fccfMQTTcp

Publish to the local broker. Command **c2f** is part of the message.
Change the hostname **Eos** in the topic to the hostname of your system.

```bash
mosquitto_pub -t ESEiot/1920sep/Eos/Tc/tc/command  -m "c2f 23"
```

### Subscribe for fccfMQTTcp

Subscribe to the local broker.
The hostname **Eos** in the topic text is changed to the hostname of your system.

```bash
mosquitto_sub -t ESEiot/1920sep/#

ESEiot/1920sep/Eos/Tc/tc/command/c2f/return 73.400000
```

---
