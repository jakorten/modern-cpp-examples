#include "Rectangle.h"

#include <iostream>

using namespace std;

std::ostream &operator<<(std::ostream &os, const Rectangle &rhs)
{
   os << "[" << rhs.width_ << "," << rhs.height_ << "]";

   return os;
}

// Rectangle::Rectangle()
//    : Rectangle{1.0, 1.0}   // Constructor delegation
// {
// }

Rectangle::Rectangle(double width, double height)
   : width_{width}
   , height_{height}
{
}

double Rectangle::getWidth() const
{
    return width_;
}

void Rectangle::setWidth(double width)
{
   width_ = width;
}
    
double Rectangle::getHeight() const 
{
    return height_;
}

void Rectangle::setHeight(double height)
{
   height_ = height;
}
    
double Rectangle::surfaceArea() const
{
  return width_ * height_;
}
