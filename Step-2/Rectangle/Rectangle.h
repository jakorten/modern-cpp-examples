#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <iostream>

// Class Rectangle represents a rectangle.
// A rectangle has 2 attributes: width and height.
// Some updates: constructor delegation, output stream operator<< overloading
// by using a friend function (not a class member function!).
// A friend function is defined outside the class, but it can use all private
// and protected members of the class.
class Rectangle
{
   // Function overloading << left shift bit operator
   // Every operator is a function!
   // rhs = right hand side of << operator:  cout << rhs
   // cout is the left hand side (lhs) of the << operator
   friend std::ostream &operator<<(std::ostream &os, const Rectangle &rhs);

public:
   // default ctor
   Rectangle() = default;
   // overloaded constructor == function overloading
   Rectangle(double width, double height);

   // setters and getters
   double getWidth() const;
   void setWidth(double width);
   double getHeight() const;
   void setHeight(double height);

   double surfaceArea() const;

private:
   // C++11: in-class initialisation
   double width_{1.0};
   double height_{1.0};
};

#endif
