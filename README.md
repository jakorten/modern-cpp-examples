# Modern C++, introduction IoT MQTT, RPi and SenseHAT in 7 steps

Several standards for C++ exist: C++03 (standard 2003), C++11 (standard 2011),
C++14 and C++17. In almost all language details these standards are backwards
compatible.

C++17 is used for compiling (g++ v7.3.0) the code examples.

Every code example can be compiled by the next Linux command.

If Makefile is available:

    make

If Makefile2 is available:

    make -f Makefile2

For cleaning, if Makefile is available:

    make clean

For cleaning, if Makefile2 is available:

    make -f Makefile2 clean
    
    make -f Makefile2 clean-depend

The make command will read the Makefile2 and will start compiling and linking according to the dependencies and commands in the Makefile. The compiled code can be debugged because the *-g* option in the Makefiles is used.

Some *dbg* launches are added to the Visual Studio Code file *launch.json* in the *.vscode* directory.

A limited number of code examples can still be compiled in the QtCreator IDE (deprecated) if .pro file is available.
Or by the command line using qmake for creating the file Makefile and make for compiling and linking:

    qmake
    make

## Fetching updated code examples

Go to the directory where you have already downloaded an initial version of the repository. Use the next git command for fetching updated code in the remote repository:

    git pull

## PlantUML

PlanUML is an open-source tool allowing users to create UML diagrams from a plain text language. It uses Graphviz to layout its diagrams. A lot of IDEs have PlantUML plugins available. Doxygen can create UML diagrams after the *\startuml* command.

## Introduction

This directory contains several basic C++ code examples necessary for
understanding the code examples in Step 1, 2, etc.

Introduction C++:

* class, object, object instantiation
* visibility: *private*, *public*
* constructor, default constructor, constructor overloading, constructor initialisation list
* class member data, class member functions, setters, getters, *const*, const-correctness
* function overloading
* call by reference
* implementation file, header file, include guards
* a class should have a single responsibility
* output stream, *cerr*, *cout*, *endl*
* single line comments *//*

Modern C++:

* *std::array* template class (container class), wraps a static array
* copy constructor, shallow and deep copy
* uniform initialisation, using *{ }*
* range based for loop, higher level of abstraction
* *auto* (automatic deduction of the data type from its initialisation expression)
* *auto*: by reference add *&*, by value nothing to add, const correct add *const*

## Step 1

C++:

* namespaces (to prevent name clashes in larger programs)
* *std* namespace
* *std::string*, formatting, conversions (string to int), basic exception handling, *std::exception*

## Step 2

C++:

* constructor delegation
* output stream operator overloading
* container classes, iterators, *begin()*, *end()*, *cbegin()*, *cend()*
* unit tests *split()* function, using [Catch2](https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md) 
* lambda functions (local functions), [algorithms](http://www.cplusplus.com/reference/algorithm/) 
* *this* pointer, *static* data and functions in classes

## Step 3

C++:

* Inheritance (public, protected, private for access control), public inheritance  is commonly used, base class, derived class
* IS-A relation, HAS-A relation (composite and aggregate)
* Abstract base classes (ABC), virtual abstract functions *= 0*
* STL pairs (data members first and second) and maps (associative container, sorted key values)
* functors, *std::function*, *bind()*

## Step 4

C++:

* SenseHAT and Pixel class (in _libSenseHAT), user defined classes, a class should have a single responsibility
* LedMatrix class (in _libSenseHAT) uses Pixel, *static const* data members
* LedMatrix wraps low level C functions in the LedMatrixDevice module, use *extern "C" { }*
* sensor reading, e.g. *SenseHAT::get_pressure()*, *SenseHAT::get_humidity()*

## Step 5

C++:

* MQTT client class based on Mosquitto base class
* RAII (Resource Aquisition Is Initialisation)
* MQTT RAII: in ctor *connect()* and in dtor *disconnect()*
* RoombaSerialLinkDemo based on [Boost Asio C++ lib](https://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/overview/serial_ports.html)

WebApp:

* MQTT web application (HTML, CSS, JS, [Bootstrap](https://getbootstrap.com/) framework), websocket connection (full-duplex communication channel over a single TCP connection)

## Step 6

C++:

* SenseHAT demo RandomWalk of leds: multi-threaded, shared device use *mutex*
* ParLoop class (executes loop in thread, uses callback function)
* *std::chrono* (stronger typing of time data, *std::chrono::duration* and *std::chrono::time_point*)

## Step 7

C++:

* Typecasting: avoid C type casting, use C++ strong-typed casts
* Summary and overview
