#include <array>
#include <iostream>
#include <vector>

// Avoid C #define in C++ use const
const int SIZE{10};

// C++ Standard Template Library: STL
// Some STL container classes: vector, list, forward_list, queue and array.
// Iterators are available for all container classes!
// Iterators: generic interface for containers.

int main()
{
   // Raw C array
   // double data[SIZE] = {10, -10, 20, -20};
   // Uniform initialization, no assignment operator
   double data[SIZE]{10, -10, 20, -20, 30}; // all resulting elements 0

   // Uniform initialization is more type stricter then = 

   // C+11 auto: compiler will deduce type of iterator
   // auto iterData = std::begin(data);  
   // Uniform initialazation:
   auto iterData{std::begin(data)};

   // Abstraction: you do not need to use SIZE for the loop anymore!
   // Argument-dependent lookup (ADL): because data is not in the std namespace
   // we need tp prefix the end() function by std::
   while (iterData != std::end(data)) {
      std::cout << *iterData << " ";
      ++iterData;
   }
   std::cout << "\n";

   // Uniform initialisation, container object, constructor takes
   // an C++11 initializer_list<int> parameter {10, 20, 30, 40}
   // and the ctor needs a {} pair ==> {{........}}
   std::array<int, SIZE> arr{{10, 20, 30, 40}};

   // C++03: array<int, 4>::iterator iter1 = arr.begin();
   // C++14 auto declaration: auto iter = cbegin(arr);   for const iterator
   // ADL: we do not need to prefix cbegin by std:: because arr is in std::
   auto iter1{cbegin(arr)};
   // Avoid this coding style: auto iter1 = arr.begin(); 
   // because you can not use it for raw C arrays!

   // Iterators have pointer semantics: operators * ++ -- + - []
   std::cout << *iter1 << std::endl;
   ++iter1;
   std::cout << *iter1 << std::endl;
   iter1 += 2;
   std::cout << iter1[0] << std::endl << std::endl;

   iter1 = begin(arr);
   while (iter1 < end(arr)) {
      std::cout << *iter1 << " ";
      ++iter1;
   }
   std::cout << std::endl;

   // C++11 range-based for loop, element is a local variable in this loop.
   // element is not the index!
   // More abstraction: prefer coding using range-based for-loops!
   for (auto element : arr) {
      std::cout << element << " ";
   }
   std::cout << std::endl;
   // element by reference in range-based for-loop, for changing the value
   // of arr elements.
   for (auto &element : arr) {
      element += 123;
   }
   for (auto element : arr) {
      std::cout << element << " ";
   }
   std::cout << std::endl << std::endl;

   //----------------------------------------------------------------------------
   std::vector<int> vec{{100, 200, 300, 400, 500}};
   // All container objects can be used by iterators in the same way.
   auto iter2{begin(vec)};

   std::cout << *iter2 << std::endl;
   ++iter2;
   std::cout << *iter2 << std::endl;
   iter2 += 2;
   std::cout << *iter2 << std::endl;

   iter2 = begin(vec);
   while (iter2 < end(vec)) {
      std::cout << *iter2 << " ";
      ++iter2;
   }
   std::cout << std::endl;

   // Prefer range based for loop, less lines of code
   for (auto e : vec) {
      std::cout << e << " ";
   }
   std::cout << std::endl;

   for (auto &e : vec) {
      e += 123;
   }
   for (auto e : vec) {
      std::cout << e << " ";
   }
   std::cout << std::endl << std::endl;

   return 0;
}
